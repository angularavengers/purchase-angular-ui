import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { HeadersComponent } from './global/headers/headers.component';
import { NavigationComponent } from './global/navigation/navigation.component';


@NgModule({
  declarations: [
    AppComponent,
    HeadersComponent,
    NavigationComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
